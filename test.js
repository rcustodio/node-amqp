'use strict';

const Connection = require('./lib/connection');
const Channel = require('./lib/channel');
const Exchange = require('./lib/exchange');

let connection = new Connection({
  host: 'localhost'
});
connection.once('ready', () => {
  console.log('connection ready');

  let channel = new Channel(connection, 1);
  channel.once('open', () => {
    console.log('channel open');
    let exchange = new Exchange(connection, channel, 'teste', {
      durable: true,
      autoDelete: false
    });

    exchange.on('open', () => {
      console.log('exchange open');
    });
  });
});
connection.once('error', (error) => {
  console.log(error);
});

connection.connect();
